#
# When you wrote a hack that could be usefull for the project, just
# copy it in this file module.
#

package Util;

use strict;
use Exporter;
use vars qw($VERSION @ISA @EXPORT @EXPORT_OK %EXPORT_TAGS);

$VERSION     = 1.00;
@ISA         = qw(Exporter);
@EXPORT      = ();
@EXPORT_OK   = qw(mkpath);
%EXPORT_TAGS = ( DEFAULT => [qw(&mkpath)],
                 ESSENTIALS    => [qw(&mkpath)]);

#because it sucks to import File::Path
sub mkpath  { 
    my @argv = @_;
    foreach my $path (@argv) {
        if ( ! -d $path ){
            my @dirs = split(/\//, $path);
            $path =~ m/^[^\/]*/;
            my $acc = $1;
            foreach (@dirs) {
                $acc .= "$_/";
                if ( ! -d $acc ){
                    mkdir $acc || die;
                }
            }
        }
    }
}


1;
